package rw.bk.sneakers.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
public class HomeController {

    @GetMapping("/home")
    public String home(@RequestParam("page") Optional<Integer> page) {
        return "home";
    }

}
