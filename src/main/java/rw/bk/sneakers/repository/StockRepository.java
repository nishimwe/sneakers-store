package rw.bk.sneakers.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rw.bk.sneakers.models.Stock;

public interface StockRepository extends JpaRepository<Stock,Long> {
}
