package rw.bk.sneakers.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rw.bk.sneakers.models.Shoes;

@Repository
public interface ShoesRepository extends JpaRepository<Shoes,Long> {
}
